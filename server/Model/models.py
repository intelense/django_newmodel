import numpy
import cv2
import tensorflow as tf


def load_cnn_model():


    image_model =tf.keras.applications.InceptionV3(include_top=False,
                                                weights='imagenet',
                                                input_shape = (299,299,3))
    new_input = image_model.input
    hidden_layer = image_model.layers[-1].output
    base_model = tf.keras.Model(new_input,hidden_layer)
    model_1 = tf.keras.Sequential([base_model,
                                tf.keras.layers.GlobalAveragePooling2D(),
                                ])
    return model_1



def load_lstm_model():


    model=tf.keras.models.load_model("trained_model_final_1.hdf5")
    model.compile(loss='binary_crossentropy', ### loss choice for category classification - computes probability error
                optimizer='adam',               ### Adam optimization
                metrics=['accuracy'])
    return model