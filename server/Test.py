import numpy
import os
import re
import pickle
import timeit
import glob
import cv2
import tensorflow as tf

from server.settings import cnn_model,lstm_model






cap = cv2.VideoCapture('http://192.168.6.118:8080/videofeed')

success,image = cap.read()
count = 0       ### start a counter at zero
error = ''      ### error flag
success = True



while True:
	success,img = cap.read()
	count += 1
	features = []
	for j in range(0,60):
		success, img = cap.read()
		if success==False:
			for i in range(0,60-j):
				features.append(batch_features)
			break
		cv2.imshow('frame',img)
		cv2.waitKey(1)
		img = load_image(img)
		batch_features = cnn_model(img)
		#batch_features = tf.reshape(batch_features,(batch_features.shape[0], -1, batch_features.shape[3]))
		batch_features = tf.squeeze(batch_features,axis=0)
		print(batch_features.shape)
		features.append(batch_features)
		#features = numpy.array(features)
		print(len(features))
		#print(success)
		#print(j)

	frames = tf.expand_dims(features, axis = 0)
	print(frames.shape)
	print('predicting...')
	prediction = lstm_model.predict(frames)
	if prediction < 0.6 :
		print ("Normal")
	else:
		print ("Caution")
	
	break
cap.release()
cv2.destroyAllWindows()