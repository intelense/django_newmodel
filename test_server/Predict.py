import numpy
import cv2
import tensorflow as tf

from server.settings import cnn_model,lstm_model




def load_image(image):
    img = tf.image.resize(image, (299, 299))
    img = tf.keras.applications.inception_v3.preprocess_input(img)
    img = tf.reshape(img,(1,299,299,3))
    features = cnn_model(img)
    features = tf.squeeze(features,axis=0)
    print("load_image done")
    return features




def predict_func(current_frame):

        frames_stack = tf.expand_dims(current_frame, axis = 0)
        print("predicting")
        prediction = lstm_model(frames_stack)
        print(prediction)
        if prediction < 0.6:
            print("Caution")
        else:
            print("Normal")
               






