from django.apps import AppConfig


class TestServerConfig(AppConfig):
    name = 'test_server'
