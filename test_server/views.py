
from django.shortcuts import render
import numpy
import os
import re
import pickle
import timeit
import glob
import cv2
import tensorflow as tf

from server.settings import cnn_model,lstm_model

from urllib.request import Request
import json
from django.http import HttpResponseRedirect , HttpResponse
from rest_framework.decorators import api_view
import requests


cap = cv2.VideoCapture('http://192.168.6.118:8080/videofeed')

success,image = cap.read()
count = 0       ### start a counter at zero
error = ''      ### error flag
success = True


def load_image(image):
    #img = tf.io.read_file(image_path)
    #img = tf.image.decode_jpeg(img, channels=3)
    img = tf.image.resize(image, (299, 299))
    img = tf.keras.applications.inception_v3.preprocess_input(img)
    img = tf.reshape(img,(1,299,299,3))
    return img


@api_view(['POST','GET'])
def predict(request):
    if request.method == "GET":
        while True:
            cap = cv2.VideoCapture('http://192.168.6.118:8080/videofeed')
            success,img = cap.read()
            global  count 
            count += 1
            # global features
            features = []
            for j in range(0,60):
                success, img = cap.read()
                print (success)
                # if success==False:
                #     for i in range(0,60-j):
                #         features.append(batch_features)
                #     break
                cv2.imshow('frame',img)
                cv2.waitKey(1)
                img = load_image(img)
                batch_features = cnn_model(img)
                #batch_features = tf.reshape(batch_features,(batch_features.shape[0], -1, batch_features.shape[3]))
                batch_features = tf.squeeze(batch_features,axis=0)
                print(batch_features.shape)
                features.append(batch_features)
                #features = numpy.array(features)
                print(len(features))
		        
            frames = tf.expand_dims(features, axis = 0)
            print(frames.shape)
            print('predicting...')
            prediction = lstm_model.predict(frames)
            if prediction < 0.6 :
                res = requests.post('http://localhost:8000/Data', json={"mask":"Normal"})
                print ("Normal")
            else:
                res = requests.post('http://localhost:8000/Data', json={"mask":"Caution"})
                print ("Caution")
            cap.release()
            cv2.destroyAllWindows()
            break

    return HttpResponse("Hello")



